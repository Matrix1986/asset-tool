const argv = require('yargs/yargs')(process.argv.slice(2))
    .usage('Usage: $0 <cmd> [options]')
    .command('init', 'init asset')
    .command('list', 'list assets')
    .command('mint', 'mint asset')
    .option('p', {
        array: false,
        description: 'the provider address of the api',
        alias: 'provider'
    })
    .option('n', {
        array: false,
        description: 'the name of the asset',
        alias: 'name'
    })
    .option('s', {
        array: false,
        description: 'the raw seed of the account',
        alias: 'seed'
    })
    .option('a', {
        array: false,
        description: 'the total amount of the token to issue',
        alias: 'amount'
    })
    .option('t', {
        array: false,
        description: 'the address of the mint to',
        alias: 'to'
    })
    .option('h', {
        alias: 'help',
        description: 'display help message'
    })
    .help('help')
    .version('0.0.1', 'version', 'display version information') // the version string.
    .alias('version', 'v')
    .showHelpOnFail(true, 'whoops, something went wrong! run with --help')
    .argv;

async function main() {
    const { Api } = require('@cennznet/api');
    const { Keyring } = require('@polkadot/keyring');

    let provider = 'ws://127.0.0.1:9944';
    if (!!argv['provider']) {
        provider = argv['provider'];
    }
    const api = await Api.create({ provider });
    const keyring = new Keyring({ type: 'sr25519' });

    let uri;
    let assetID;
    const assetName = argv['name'] ?? 'TEST';

    const assets = await api.rpc.genericAsset.registeredAssets();
    assets.forEach(([id, info]) => {
        if (String.fromCharCode.apply(null, info.symbol) === assetName) {
            assetID = id;
        }
    });

    if (!argv['seed']) {
        uri = '//Alice';
    } else {
        if (argv['seed'].length !== 32) {
            console.error("seed must be 32 characters");
            process.exit(1);
        } else {
            uri = '0x' + Buffer.from(argv['seed'], 'utf-8').toString('hex');
        }
    }

    const assetOwner = keyring.addFromUri(uri);

    switch (argv['_'][0]) {
        case 'init':
            if (!!assetID) {
                console.error("The asset exists, just skip.");
                process.exit(1);
            }

            if (!argv['amount']) {
                console.error('amount must be set.');
                process.exit(1);
            }

            const initialIssuance = argv['amount'] * 1;
            if (isNaN(initialIssuance)) {
                console.error('amount must be a number');
                process.exit(1);
            }

            const permissions = {
                update: {
                    Address: assetOwner.address
                },
                mint: {
                    Address: assetOwner.address
                },
                burn: {
                    Address: assetOwner.address
                },
            };

            const options = { initialIssuance, permissions };
            const metadata = { symbol: assetName, decimalPlaces: 4, existentialDeposit: 5 };

            await api.tx.genericAsset.create(
                assetOwner.address,
                options,
                metadata,
            ).signAndSend(assetOwner);
            console.log(`Asset ${assetName} initialized`);
            break;

        case 'list':
            assets.forEach(([id, info]) => {
                console.log(`${id}:` + String.fromCharCode.apply(null, info.symbol));
            });

            const bob = keyring.addFromUri('//Bob');
            console.log(`${bob.address}`);
            break;

        case 'mint':
            if (!assetID) {
                console.error("The asset non exists, use init command to initialize it first.");
                process.exit(1);
            }

            if (!argv['to']) {
                console.error('mint destination must be set.');
                process.exit(1);
            }

            if (!argv['amount']) {
                console.error('mint amount must be set.');
                process.exit(1);
            }

            await api.tx.genericAsset
                .mint(assetID, argv['to'], argv['amount'])
                .signAndSend(assetOwner);
            break;
    }
}

main().catch(console.error).finally(() => process.exit(0));