## Setup

- `yarn install`
- `docker pull cennznet/cennznet:latest`
- ` docker run --rm -p 9944:9944 cennznet/cennznet:latest --dev --ws-external`



## How to use

### Help

`node .\bin\index.js -h`

### Generate a new asset

The command blow will generate a new asset named 'GOLD' and with 10000000 initail balance with the owner

**Default owner is Alice.**

You should use -s option to provide custom private key seed with 32 length characters

`node .\bin\index.js init -n GOLD -a 10000000`

### List assets

`node .\bin\index.js list`



### Mint asset

`node .\bin\index.js mint -n GOLD -t ${address}`
